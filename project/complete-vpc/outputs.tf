# VPC
output "vpc_id" {
  description = "The ID of the VPC"
  value       = "${module.vpc.vpc_id}"
}

#INSTANCES
output "sharepoint_webinstance_AZ1_public_ip" {
  description = "This is the sharepoint webinstance 1 Public IP"
  value       = "${module.vpc.sharepoint_webinstance_AZ1_public_ip}"
}
output "sharepoint_webinstance_AZ2_public_ip" {
  description = "This is the sharepoint webinstance 2 Public IP"
  value       = "${module.vpc.sharepoint_webinstance_AZ2_public_ip}"
}
output "sharepoint_appinstance_AZ1_private_ip" {
  description = "This is the sharepoint appinstance 1 Private IP"
  value       = "${module.vpc.sharepoint_appinstance_AZ1_private_ip}"
}
output "sharepoint_appinstance_AZ2_private_ip" {
  description = "This is the sharepoint appinstance 2 Private IP"
  value       = "${module.vpc.sharepoint_appinstance_AZ2_private_ip}"
}
output "sharepoint_dbinstance_AZ1_private_ip" {
  description = "This is the sharepoint dbinstance 1 Private IP"
  value       = "${module.vpc.sharepoint_dbinstance_AZ1_private_ip}"
}
output "sharepoint_dbinstance_AZ2_private_ip" {
  description = "This is the sharepoint dbinstance 2 Private IP"
  value       = "${module.vpc.sharepoint_dbinstance_AZ2_private_ip}"
}
output "sharepoint_adinstance_AZ1_private_ip" {
  description = "This is the sharepoint AD instance 1 Private IP"
  value       = "${module.vpc.sharepoint_adinstance_AZ1_private_ip}"
}
output "sharepoint_adinstance_AZ2_private_ip" {
  description = "This is the sharepoint AD instance 2 Private IP"
  value       = "${module.vpc.sharepoint_adinstance_AZ2_private_ip}"
}
output "sharepoint_bastion_public_ip" {
  description = "This is the sharepoint Bastion Public IP"
  value       = "${module.vpc.sharepoint_bastion_rdp_public_ip}"
}


# Subnets
output "private_subnets" {
  description = "List of IDs of private subnets"
  value       = ["${module.vpc.private_subnets}"]
}

output "public_subnets" {
  description = "List of IDs of public subnets"
  value       = ["${module.vpc.public_subnets}"]
}

#output "database_subnets" {
#  description = "List of IDs of database subnets"
#  value       = ["${module.vpc.database_subnets}"]
#}

#output "elasticache_subnets" {
#  description = "List of IDs of elasticache subnets"
#  value       = ["${module.vpc.elasticache_subnets}"]
#}

#output "redshift_subnets" {
#  description = "List of IDs of redshift subnets"
#  value       = ["${module.vpc.redshift_subnets}"]
#}

# NAT gateways
output "nat_public_ips" {
  description = "List of public Elastic IPs created for AWS NAT Gateway"
  value       = ["${module.vpc.nat_public_ips}"]
}

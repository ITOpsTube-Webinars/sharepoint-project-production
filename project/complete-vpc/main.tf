provider "aws" {
  region = "us-east-1"
}

module "vpc" {
  source = "../../"
  name = "Sharepoint-Production"
  cidr = "10.240.146.64/26"

  azs                 = ["us-east-1a", "us-east-1b"]
  private_subnets     = ["10.240.146.96/28", "10.240.146.112/28"]
  public_subnets      = ["10.240.146.64/28", "10.240.146.80/28"]

  create_database_subnet_group = false

  enable_nat_gateway = true
  enable_vpn_gateway = true

  enable_s3_endpoint       = true

  enable_dhcp_options              = true
  dhcp_options_domain_name         = "service.consul.prod"
  dhcp_options_domain_name_servers = ["127.0.0.1", "10.240.146.66"]

  tags = {
    Owner       = "NIH"
    Environment = "Production"
    Name        = "Sharepoint"
  }
}

